# README #

Samotná třída zebra je napsána v PHP jazyce a má zejména čtyři veřejné metody. První metoda putValue(číslo domu, typ hodnoty, hodnota) zařazuje do matice predikáty, které jsou všeobecně známe. Po sestavení známých predikátu vygenerujeme matici pomoci metody generateTable(). Následně můžeme pokoušet zařazovat jednotlivé neznámé predikáty pomocí metody putUndefined(hodnota, typ hodnoty). 
Po zavolání teto metody se snaží script neznáme hodnoty zařadit do matice tak, aby neporušil platnost jiných predikátů. Pokud není schopen patřičně zařadit, vyhlásí exception a uživatele informuje.
Poté pro dohledání konkrétní hodnoty můžeme využít metodu searchValue(), která vrátí patřičné zařazení hodnoty.
