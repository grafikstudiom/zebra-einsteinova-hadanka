<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 30.04.2017
 * Time: 19:03
 */
class Zebra
{
    const COLOR = 'color';
    const NATIONAL = 'national';
    const DRINK = 'drink';
    const SMOKE = 'smoke';
    const ANIMAL = 'animal';

    private $available = ['color', 'national', 'drink', 'smoke', 'animal'];
    private $color = ['Žlutá', 'Modrá', 'Červená', 'Zelená', 'Bílá'];
    private $national = ['Nor', 'Dán', 'Angličan', 'Němec', 'Švéd'];
    private $drink = ['Voda', 'Čaj', 'Mléko', 'Káva', 'Pivo'];
    private $smoke = ['Dunhill', 'Blend', 'Pall Mall', 'Prince', 'Blue Master'];
    private $animal = ['Kočka', 'Koně', 'Ptáci', 'Ryby', 'Pes'];

    private $house = [1 => [], 2 => [], 3 => [], 4 => [], 5 => []];

    public function putValue(string $house, string $type, string $value) : bool
    {
        if(!in_array($type, $this->available)){
            throw new Exception(sprintf('Type is not available: House: %s, type: %s, value: %s', $house, $type, $value));
            return false;
        }

        if(!empty($this->house[$house][$type])){
            var_dump($this->house[$house][$type]);
            throw new Exception(sprintf('This value in house is used!: House: %s, type: %s, value: %s', $house, $type, $value));
            return false;
        }

        if(array_search($value, $this->$type) === false){
            throw new Exception(sprintf('This value is used!: House: %s, type: %s, value: %s', $house, $type, $value));
            return false;
        }

        $key = array_search($value, $this->$type);
        $this->house[$house][$type] = $this->$type[$key];
        unset($this->$type[$key]);
        return true;
    }

    public function putUndefined($type, $value) : bools
    {
        foreach($this->house as $num => $house){
            if(count($house) < 5 && !isset($house[$type])){
                $this->house[$num][$type] = $value;
                return true;
            }
        }
        throw new Exception('Incorrect assignment!');
    }

    public function searchValue($input, $type)
    {
        foreach($this->house as $num => $house){
            foreach ($house as $key => $value){
                if($value === $input){
                    echo sprintf("%s => House: %s, %s: %s", $input, $num, $type, $house[$type]);
                }
            }
        }
    }

    public function generateTable()
    {
        var_dump($this->house);
    }
}