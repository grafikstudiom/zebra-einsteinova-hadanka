<?php
/**
 * Created by PhpStorm.
 * User: marti
 * Date: 30.04.2017
 * Time: 19:03
 */

require 'Zebra.php';

$zebra = new Zebra();

try {
    $zebra->putValue(1, Zebra::NATIONAL, 'Nor');
    $zebra->putValue(2, Zebra::COLOR, 'Modrá');
    $zebra->putValue(3, Zebra::DRINK, 'Mléko');
    $zebra->putValue(1, Zebra::COLOR, 'Žlutá');
    $zebra->putValue(4, Zebra::COLOR, 'Zelená');
    $zebra->putValue(4, Zebra::DRINK, 'Káva');
    $zebra->putValue(5, Zebra::COLOR, 'Bílá');
    $zebra->putValue(1, Zebra::SMOKE, 'Dunhill');
    $zebra->putValue(2, Zebra::ANIMAL, 'Koně');
    $zebra->putValue(2, Zebra::NATIONAL, 'Dán');
    $zebra->putValue(2, Zebra::DRINK, 'Čaj');
    $zebra->putValue(5, Zebra::SMOKE, 'Blue Master');
    $zebra->putValue(3, Zebra::COLOR, 'Červená');
    $zebra->putValue(4, Zebra::NATIONAL, 'Němec');
    $zebra->putValue(5, Zebra::DRINK, 'Pivo');
    $zebra->putValue(4, Zebra::SMOKE, 'Prince');
    $zebra->putValue(5, Zebra::NATIONAL, 'Švéd');
    $zebra->putValue(5, Zebra::ANIMAL, 'Pes');
    $zebra->putValue(3, Zebra::NATIONAL, 'Angličan');
    $zebra->putValue(3, Zebra::SMOKE, 'Pall Mall');
    $zebra->putValue(3, Zebra::ANIMAL, 'Ptáci');
    $zebra->putValue(1, Zebra::ANIMAL, 'Kočka');
    $zebra->putValue(1, Zebra::DRINK, 'Voda');
    $zebra->putValue(2, Zebra::SMOKE, 'Blend');

    $zebra->generateTable();
    $zebra->putUndefined(Zebra::ANIMAL, 'Ryby');
    //$zebra->generateTable();
} catch(Exception $e){
    echo $e->getMessage();
}
    echo $zebra->searchValue('Ryby', Zebra::NATIONAL);